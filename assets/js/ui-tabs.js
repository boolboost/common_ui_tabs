window.addEventListener('load', function() {
  var bemClass = 'ui-tabs';
  var activeClass = 'is-active';
  var tabs = document.querySelectorAll('.' + bemClass + ' > .' + bemClass + '__header > ul > li');

  var tabClick = function (e) {
    for (var i = 0; i < tabs.length; i++) {
      tabs[i].classList.remove(activeClass);
    }

    e.currentTarget.classList.add(activeClass);
    e.preventDefault();

    var panes = document.querySelectorAll('.' + bemClass + '__pane');
    for (i = 0; i < panes.length; i++) {
      panes[i].classList.remove(activeClass);
    }

    var anchorReference = e.currentTarget.querySelector('a');
    var activePaneId = anchorReference.getAttribute('href');
    var activePane = document.querySelector(activePaneId);

    activePane.classList.add(activeClass);
  };

  for (var i = 0; i < tabs.length; i++) {
    tabs[i].addEventListener('click', tabClick);
  }
});
