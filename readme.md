###Example

``` twig
{{ attach_library('common_ui_tabs/ui_tabs') }}

<div class="ui-tabs">
  <div class="ui-tabs__header">
    <ul>
      <li class="is-active">
        <a href="#pane-1">Pane 1</a>
      </li>
      <li>
        <a href="#pane-2">Pane 2</a>
      </li>
      <li>
        <a href="#pane-3">Pane 3</a>
      </li>
      <li>
        <a href="#pane-4">Pane 4</a>
      </li>
    </ul>
  </div>

  <div class="ui-tabs__pane is-active" id="pane-1">Pane 1</div>
  <div class="ui-tabs__pane" id="pane-2">Pane 2</div>
  <div class="ui-tabs__pane" id="pane-3">Pane 3</div>
  <div class="ui-tabs__pane" id="pane-4">Pane 4</div>
</div> 
```